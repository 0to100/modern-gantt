# Modern Gantt 

### Install
```
npm install ...
```

### Usage
Include it in your HTML:
```
<script src="..."></script>
<link rel="stylesheet" href="...">

<svg id="#gantt"></svg>
```

And start hacking:
```js
var tasks = [
]
var gantt = new Gantt("#gantt", tasks);
```

If you want to contribute:

1. Clone this repo.
2. `cd` into project directory
3. `yarn`
4. `yarn run dev`

------------------
Project maintained by [0to100](https://0to100.ink)

Inspired by [Frappe Gantt](https://github.com/frappe)